(function() {

    // INIT "PARALLAX" SLIDER "SWIPER JS"
    const swiper = new Swiper('.swiper', {
        direction: 'horizontal',
        slidesPerView: 1,
        loop: true,
        speed: 1000,

        effect: 'fade',
        fadeEffect: {
            crossFade: true
        },

        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        },

        on: {

            slideChangeTransitionEnd: function() {
                let swiper = this,
                    imgs_1 = swiper.el.querySelectorAll('.img1'),
                    imgs_2 = swiper.el.querySelectorAll('.img2'),
                    imgs_3 = swiper.el.querySelectorAll('.img3'),
                    trans_btns = swiper.el.querySelectorAll('.has_transition');
                
                // REMOVE ALL TRANSITION "ANIMATION" CLASSES
                for (let i = 0; i < imgs_1.length; i++) {
                    imgs_1[i].classList.remove('show');
                    imgs_2[i].classList.remove('show');
                    imgs_3[i].classList.remove('show');
                    trans_btns[i].classList.remove('animate');
                }

                // ADD TRANSITION "ANIMATION" CLASS FOR "THIS ACTIVE" SLIDE 
                swiper.slides[swiper.activeIndex].querySelector('.img1').classList.add('show');
                swiper.slides[swiper.activeIndex].querySelector('.img2').classList.add('show');
                swiper.slides[swiper.activeIndex].querySelector('.img3').classList.add('show');
                swiper.slides[swiper.activeIndex].querySelector('.btn.has_transition').classList.add('animate');
            
            }
        }
    })


})()